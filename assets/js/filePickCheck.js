const fileInput1 = document.getElementById('file1');
const fileInput2 = document.getElementById('file2');
const fileInput3 = document.getElementById('file3');
let flag1 = 0;
let flag2 = 0;
let flag3 = 0;
fileInput1.addEventListener('change', function(e){
   
    let fileName = this.files[0].name;
    let fileExt = fileName.split('.')[fileName.split('.').length - 1];
    // console.log(fileExt);
    if(fileExt === 'jpg' || fileExt === 'jpeg' || fileExt === 'png' || fileExt === 'gif'){
        flag1 = 1;
    }
    else{
        flag1 = 0;
    }
});

fileInput2.addEventListener('change', function(e){
    let fileName = this.files[0].name;
    let fileExt = fileName.split('.')[fileName.split('.').length - 1];
    if(fileExt === 'fbx' || fileExt === 'obj'){
        flag2 = 1;
    }
    else{
        flag2 = 0;
    }
});

fileInput3.addEventListener('change', function(e){
    let fileName = this.files[0].name;
    let fileExt = fileName.split('.')[fileName.split('.').length - 1];
    if(fileExt === 'zip' || fileExt === 'rar'){
       flag3 = 1;
    }
    else{
        flag3 = 0;
    }
});

let button =  document.getElementById('uploadbutton');
button.addEventListener('click', function(e){
    e.preventDefault();
    if(flag1 === 1 && flag2 === 1 && flag3 === 1){
        document.getElementById('myform').submit();
    }
    else{
        new Noty({
            theme: 'relax',
            text: "Check file extensions!",
            type: 'success',
            layout: 'topRight',
            timeout: 1500
            
        }).show();
    }
})

