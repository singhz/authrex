const aws = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");
const path = require('path');

aws.config.update({
  accessKeyId: "AKIAQSID4DRVVEZ5ZJ4Q",
  secretAccessKey: "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",
  region: "us-east-2",
});

const s3 = new aws.S3();

const fileFilter = (req, file, cb) => {
  // console.log(req);
    // let ext = path.extname(file.originalname);
    // let flag = 0; 
    // console.log(path.extname(file.originalname));
    // if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
    //   flag = 1;
    //   return cb(new Error('Only images are allowed'));
    // }
    // if(flag == 0 && ext !== '.fbx' && ext !== '.obj') {
    //   return cb(new Error('Only models are allowed'));
    // }
    // if(ext !== '.zip' && ext !== '.rar') {
    //   return cb(new Error('Only compressed files are allowed'));
    // }
    cb(null, true);
};

const upload = multer({
  fileFilter,
  storage: multerS3({
    s3: s3,
    bucket: "singhz-intern",
    acl: "public-read",
    metadata: function (req, file, cb) {
      cb(null, { fieldName: "Testing meta-data" });
    },
    key: function (req, file, cb) {
      cb(null, `folder/${req.user.email}/${Date.now().toString()}.${file.originalname.split('.')[file.originalname.split('.').length -1]}`);
    },
  }),
  limits: {
    fileSize: 1024 * 1024 * 220,
  },
});

module.exports = upload;
