const mongoose = require("mongoose");


//======creting schema for storing usres data ==========

const modelSchema = new mongoose.Schema(
  {
    name: {
        type: String,
        required: true
    },
    modelLocation: {
        type: String,
        default: ''
    },
    texturesLocation: {
        type: String,
        default: ''
    },
    imageLocation: {
        type: String,
        default: ''
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }
  },
  {
    timestamps: true,
  }
);

const Model = mongoose.model("Model", modelSchema);

module.exports = Model;
