const upload = require("../../../config/fileUpload");
const User = require('../../../models/user');
const Model = require('../../../models/model');

// const singleUpload = upload.single("image");

const multipleUpload = upload.array("models", 10);

module.exports.fileUpload = async function(req, res){
    
    try{
       multipleUpload(req, res, function (err) {
            if (err) {
              return res.status(422).send({
                errors: [{ title: "File Upload Error", detail: err.message }],
              });
            }
            let imageLocation = req.files[0].location;
            imageLocation = imageLocation.substr(49, imageLocation.length - 49);
            let modelLocation = req.files[1].location;
            modelLocation = modelLocation.substr(49, modelLocation.length - 49);
            let texturesLocation = req.files[2].location;
            texturesLocation = texturesLocation.substr(49, texturesLocation.length - 49);

             Model.create({
                name: req.body.modelName, 
                modelLocation: modelLocation,
                texturesLocation: texturesLocation,
                imageLocation : imageLocation,
                user: req.user._id,
            })
            
            // return res.json({
            //   imageUrl: '',
            //   properties: req.files,
            // });
            
            // return res.render('home', {
            //   title: "Home",
            //   imageurl : imageLocation,
            // })
            req.flash("success", "files have been successfully uploaded");
            return res.redirect('back');
          });
    }
    catch(err){
        return res.redirect('back');
    }
}