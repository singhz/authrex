const express = require("express");

const router = express.Router();

router.use("/home", require("./home"));
router.use("/upload", require("./file-upload"));
module.exports = router;
