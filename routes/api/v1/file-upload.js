const express = require("express");
const router = express.Router();

const fileUploadController = require('../../../controllers/api/v1/file-upload');
router.post("/image-upload", fileUploadController.fileUpload);

module.exports = router;
